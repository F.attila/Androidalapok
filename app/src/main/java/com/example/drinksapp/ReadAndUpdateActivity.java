package com.example.drinksapp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;
import android.widget.EditText;

import java.util.concurrent.Executors;

public class ReadAndUpdateActivity extends AppCompatActivity {
    private Cocktail cocktail;
    private CocktailDao cocktailDao;
    private int pos;
    MyRecyclerViewAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "cocktail-database").build();
        cocktailDao = db.cocktailDao();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_and_update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        cocktail = (Cocktail) i.getSerializableExtra("cocktail");
        pos = i.getIntExtra("pos", 0);
        CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolbarLayout.setTitle(cocktail.name);
        EditText textView = (EditText) findViewById(R.id.editTextTextMultiLine);
        textView.setText(cocktail.description);

    }
    @Override
    protected void onPause(){
        super.onPause();
        EditText textView = (EditText) findViewById(R.id.editTextTextMultiLine);
        cocktail.description = textView.getText().toString();
        Executors.newSingleThreadExecutor().execute(() -> updateCocktail(cocktail));
        super.finish();

    }
    private void updateCocktail(Cocktail c){
        cocktailDao.updateCocktails(c);

    }


}