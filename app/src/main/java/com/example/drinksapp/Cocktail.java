package com.example.drinksapp;

import java.io.Serializable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

@Entity(tableName = "cocktail_table")
public class Cocktail implements Serializable{


    @Ignore
    Cocktail(String name, String description){
        this.name = name;
        this.description = description;
    }

    Cocktail(int cocktailId, String name, String description){
        this.cocktailId = cocktailId;
        this.name = name;
        this.description = description;
    }





    @PrimaryKey(autoGenerate = true)
    public int cocktailId;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "description")
    public String description;



    public String toString(){
        return this.name;
    }
}
