package com.example.drinksapp;

import java.util.List;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


@Dao
public interface CocktailDao {
    @Query("SELECT * FROM cocktail_table")
    List<Cocktail> getAll();

    @Query("SELECT * FROM cocktail_table WHERE cocktailId IN (:cocktailIds)")
    List<Cocktail> findById(int[] cocktailIds);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCocktails(Cocktail... cocktails);

    @Update
    public void updateCocktails(Cocktail... cocktails);

    @Delete
    void delete(Cocktail cocktail);
}
