package com.example.drinksapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;



public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener{
    private CocktailDao cocktailDao;
    ArrayList<Cocktail> dictionaryItems;
    MyRecyclerViewAdapter adapter;
    private static final int REQUEST_CODE = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "cocktail-database").build();
        cocktailDao = db.cocktailDao();
        dictionaryItems = new ArrayList<>();
        Executors.newSingleThreadExecutor().execute(() -> getCocktails());


        RecyclerView recyclerView = findViewById(R.id.dictionary);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new MyRecyclerViewAdapter(this, dictionaryItems);
        adapter.setClickListener(this);


        recyclerView.setAdapter(adapter);

    }
    @Override
    protected void onResume() {

        super.onResume();
        dictionaryItems.clear();
        Executors.newSingleThreadExecutor().execute(() -> getCocktails());

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.option_add:
                showAddActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(MainActivity.this, ReadAndUpdateActivity.class);
        intent.putExtra("cocktail", adapter.getItem(position));
        intent.putExtra("pos", position);
        startActivity(intent);
    }


    @Override
    public void onButtonClick(View view, int position) {
        Executors.newSingleThreadExecutor().execute(() -> deleteCocktail((adapter.getItem(position))));

        adapter.notifyItemRangeChanged(position, dictionaryItems.size());
        adapter.notifyItemRemoved(position);
        dictionaryItems.remove(position);
    }



    private void getCocktails(){

        /*cocktailDao.insertCocktails(new Cocktail("sghsbjadd","jhdcm"));
        cocktailDao.insertCocktails(new Cocktail("sghsbjdssfdd","jhdcm"));
        cocktailDao.insertCocktails(new Cocktail("sghsbsdvcdsjadd","jhdcm"));
        cocktailDao.insertCocktails(new Cocktail("sghsbxdxcscdcjadd","jhdcm"));
        cocktailDao.insertCocktails(new Cocktail("sghsbjydvddsdgrfdsaaadd","jhdcm"));*/

        List<Cocktail> cocktailList= cocktailDao.getAll();
        for(Cocktail c : cocktailList){
            dictionaryItems.add(c);
        }

    }

    private void deleteCocktail(Cocktail c){
        cocktailDao.delete(c);

    }
    private void addCocktail(Cocktail c){
        cocktailDao.insertCocktails(c);

    }
    private void showAddActivity(){
        Intent intent = new Intent(MainActivity.this, AddActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Cocktail c = (Cocktail) data.getSerializableExtra("add");
                Executors.newSingleThreadExecutor().execute(() -> addCocktail(c));
                dictionaryItems.add(c);
                adapter.notifyItemInserted(dictionaryItems.size()-1);

            }
        }
    }

}