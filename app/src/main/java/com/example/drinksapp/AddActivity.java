package com.example.drinksapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Button secondButton = (Button) findViewById(R.id.button2);
        secondButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText name = (EditText) findViewById(R.id.editTextTextCocktailName);
                EditText description = (EditText) findViewById(R.id.editTextTextMultiLine2);
                Cocktail c = new Cocktail(name.getText().toString(), description.getText().toString());

                Intent intent = new Intent();
                intent.putExtra("add", c);

                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}